﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace BgInfo_guard
{
    public partial class frmMain : Form
    {
        public string[] CmdArgs;
        public frmMain(string[] args)
        {
            CmdArgs = args;
            InitializeComponent();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            app_notify.Visible = true;
            app_notify.Text = Application.ProductName + " " + Application.ProductVersion.ToString();

            timer_app.Enabled = true;
            timer_app.Interval = 1000 * (60 * 60);

            timer_start.Enabled = true;
            timer_start.Interval = 100;
            Application.DoEvents();
        }
        private void Execute_BGInfo()
        {
            Execute_BGInfo("");
        }
        private void Execute_BGInfo(string BGI_file) {
            if (BGI_file == "") BGI_file = "Bginfo.bgi";
            
            System.Diagnostics.Process proc; // Declare New Process
            System.Diagnostics.ProcessStartInfo procInfo = new System.Diagnostics.ProcessStartInfo(); // Declare New Process Starting Information

            procInfo.UseShellExecute = true;  //If this is false, only .exe's can be run.
            procInfo.WorkingDirectory = "C:"; //execute notepad from the C: Drive
            procInfo.FileName = "C:\\Program Files\\_servis\\Bginfo.exe"; // Program or Command to Execute.
            procInfo.Arguments = "\\\\radio.cmm\\netlogon\\" + BGI_file + " /timer:0 /silent"; //Command line arguments.
            proc = System.Diagnostics.Process.Start(procInfo); // same as typing "notepad.exe C:\boot.ini" from windows Start->Run.
            proc.WaitForExit(); // Waits for the process to end. (ie. when user closes it down)
            if(!proc.HasExited) // Just To Be Safe.
            {
                proc.Kill();
            }
        }

        private void timer_start_Tick(object sender, EventArgs e)
        {
            timer_start.Enabled = false;
            this.WindowState = FormWindowState.Minimized;
            this.Visible = false;
        }

        private void timer_app_Tick(object sender, EventArgs e)
        {
            string arg = "";
            if (CmdArgs.Length > 0) { arg = CmdArgs[0]; }
            else { arg = "Bginfo.bgi"; }
            Execute_BGInfo(arg);
        }
    }
}